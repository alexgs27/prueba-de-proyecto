<!DOCTYPE html>
<html>

<head>

	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>Aulas de informática - Universidad de Granada - CSIRC</title>

	<style type="text/css">

	body {	
		font-size: 75%;
		background: url('images/fondoAULAS_cabecera.png') no-repeat center;
	  	background-color: #004D97;
	  	background-size: contain;
	}

	a:link, a:visited { text-decoration: none; color:#900; font-weight: bold; }
	a:hover, a:active { color:#d90; }
	br {margin-bottom: 0.5%;}

	h1 {
		margin-bottom: 20%;
	}

	dl {

		background: transparent no-repeat top left;
		padding: 0 0 2% 5%;
	}

	fieldset {
	    border-radius: 6px 6px 6px 6px;
	    border-color: #FFFFFF;
	    text-align: left;
	    border-width: 2px;
	    background: #0431B9;
	   # margin: 4% 34% 0% 33.5%;
	    margin: 2% 30% 0% 30%;
	    #padding: 1.5% 1.5% 2% 1.5%;
	    padding: 1.5% 1.5% 1.5% 1.5%;
	}

	img.imagen {
		margin-left: 8%;
		margin-bottom: 4%;
	}

	p {
		#font-size: 1.2em;
		font-size: 1em;
		
	}
	p.paraImagen {text-align: center;}
	p.mensajeUsuarios {font-size: 1em; color: red; text-align:center; padding-bottom: 0em; margin-bottom: 0em;}

	form.signin .textbox label {
	    display: block;
	    padding-bottom: 10px;
	}

	form.signin .textbox span {
	    display: block;
	}

	form.signin span {
	    color: #DDD;
	    font-size: 1.2em;
	    line-height: 100%;
	}

	#user {
		margin-right: 6%;
	}

	#pass {
		margin-right: 1%;
	}

	.texto {
		padding-top: 2%;
		padding-right: 2%;
		padding-bottom: 16%;
		float: left;
		display: block;
	}

	.inputs {
		margin-bottom: 5%;
	}

	.inputs input {
	    background: #6DC8FF;
	    border-bottom: 1px #007;
	    border-left: 1px solid #007;
	    border-right: 1px solid #007;
	    border-top: 1px solid #007;
	    border-radius: 3px 3px 3px 3px;
	    font: 1.1em Arial, Helvetica, sans-serif;
	    color: #007;
	    padding: 4px 4px 3px;
	    margin-bottom: 2%;
	    width: 135px;
	    display: block;
	}

	.boton {
		background: #EEE;
		border-radius: 0;
		border-bottom: 1px solid #999;
	    border-left: 1px solid #999;
	    border-right: 1px solid #999;
	    border-top: 1px solid #999;
	    border-radius: 2px 2px 2px 2px;
		font: 1em Arial, Helvetica, sans-serif;
		font-weight: bold;
		color: black;
		width: 70px;
		height: 1.8em;
	}


	.dgcAlert {
		top: 0;
		position: absolute;
		width: 100%;display: block;
		height: 1000px; 
		background: url('images/fondoAlert.png') repeat; 
		text-align:center; 
		opacity:0; 
		display:none; 
		z-index:999999999999999;
	}

	.dgcAlert .dgcVentana {
		width: 500px; 
		background: white;
		min-height: 150px;
		position: relative;
		margin: 0 auto;
		color: black;
		padding: 10px;
		border-radius: 10px;
	}

	.dgcAlert .dgcVentana .dgcCerrar {
		height: 25px;
		width: 25px;
		float: right; 
		cursor:pointer; 
		background: url('images/cerrarAlert.png') no-repeat center center;
	}

	.dgcAlert .dgcVentana .dgcMensaje { 
		margin: 0 auto; 
		padding-top: 45px; 
		text-align: center; 
		width: 400px;
		font-size: 20px;
	}

	.dgcAlert .dgcVentana .dgcAceptar{
		background:#09C; 
		bottom:20px; 
		display: inline-block; 
		font-size: 12px; 
		font-weight: bold; 
		height: 24px; 
		line-height: 24px; 
		padding-left: 5px; 
		padding-right: 5px;
		text-align: center; 
		text-transform: uppercase; 
		width: 75px;cursor: pointer; 
		color:#FFF; margin-top:50px;
	}

	dl.ayuda {
		margin-left: 0%;
	}

	dl.apagar {
		margin-left: 67%;
	}

	dl.reiniciar {
		margin-top: 6%;
		margin-left: 40%;
		margin-bottom: 18%
	}

	dt { float: left;}

	</style>


	<script type="text/javascript" src="./js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript">

	/* Esta funcion sirve para tener un 'alert' personalizado, y no el tipico de javascript. 
	   Para personalizar tendríamos que modificar las clases de css: .dgcAlert, .dgcVentana, .dgcMensaje, .dgcCerrar y .dgcAceptar */
	function alertDGC(mensaje) {
	    var dgcTiempo=500
	    var ventanaCS='<div class="dgcAlert"><div class="dgcVentana"><div class="dgcCerrar"></div><div class="dgcMensaje">'+mensaje+'<br><div class="dgcAceptar">Aceptar</div></div></div></div>';
	    $('body').append(ventanaCS);
	    var alVentana=$('.dgcVentana').height();
	    var alNav=$(window).height();
	    var supNav=$(window).scrollTop();
	    $('.dgcAlert').css('height',$(document).height());
	    $('.dgcVentana').css('top',((alNav-alVentana)/2+supNav-100)+'px');
	    $('.dgcAlert').css('display','block');
	    $('.dgcAlert').animate({opacity:1},dgcTiempo);
	    $('.dgcCerrar,.dgcAceptar').click(function(e) {
	        $('.dgcAlert').animate({opacity:0},dgcTiempo);
	        setTimeout("$('.dgcAlert').remove()",dgcTiempo);
	    });

	}
	</script>

</head>

<body>

	<h1></h1>

	<form class="signin" name="form1" action="ControlAccesoUsuarioUGR.php" method="POST">
		<fieldset class="textbox">
			<p class="paraImagen"><img class="imagen" src="images/identificacion.png"></p>

			<p>PDI/PAS: <font color="#DDD">usuario</font> sin @ugr.es</p>
			<p>Alumnos: <font color="#DDD">usuario.docencia</font> sin @correo.ugr.es</p>

		<div class="texto">
		<label class="username">
		<div class="texto" id="user"><span>usuario:</span></div>
		</label>
		<label class="password">
		<div class="texto" id="pass"><span>password:</span></div>
		</label>
		</div>
		<div class="inputs">
		<input id="inputusuario" type="text" name="usuario">
		<input type="password" name="contrasena">
		</div>

		<input class="boton" type="Submit" value="Validar">
		<button class="boton" type="button" onclick="cancelar()">Cancelar</button>
		<!-- Mensaje de advertencia para usuarios -->
		<p class="mensajeUsuarios">Al terminar su sesi&oacute;n de trabajo no olvide apagar Windows/Linux. </p>
		</fieldset>
	</form>

	<dl class="ayuda">
		<dt><a href="javascript:;" title="Ayuda sobre la identificación de usuario"><img src="images/ayudaIdentificacion.png"></a></dt>

		<dl class="apagar">
			<dt><a href="command:poweroff" title="Apagar el equipo"><img src="images/apagar.png"></a></dt>
		
			<dl class="reiniciar">
				<dt><a href="command:reboot" title="Reiniciar el equipo"><img src="images/reiniciar.png"></a></dt>				
			</dl>

		</dl>
	</dl>

	<?PHP if ($_GET["errorauth"]=="error"){?> 
		<script type="text/javascript">
			try {
				alertDGC("Usuario o Contrase\u00f1a err\u00f3neos, int\u00e9ntelo de nuevo.");
			}
			catch (err) {
				alert("Usuario o Contrase\u00f1a err\u00f3neos, int\u00e9ntelo de nuevo.");
			} 
		</script>
	<?PHP }?>
</body>

<html>
