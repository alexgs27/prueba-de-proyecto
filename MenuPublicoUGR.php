<?PHP

/* Antes de nada, inicializamos las variables PHP para la correcta configuración 
del menú. Es IMPORTANTE poner el punto y coma después de cada sentencia */

/* Primero incluimos el autoload global */
include '/opt/opengnsys/www/menus/autoloads/autoload.php';

/* A continuación, tenemos que incluir el autoload del aula y el autoload del equipo (en caso de que exista).
Para ello necesitamos saber el nombre del host, que estará almacenado en un fichero con su IP de nombre.
Este fichero se crea en el autoexec arranque general, por lo que es importante que el equipo en cuestión 
tenga configurado el autoexec */
$ipCliente = $_SERVER['REMOTE_ADDR'];
$fileHostname = fopen("/opt/opengnsys/log/clients/hostnames/$ipCliente.hostname","r");
$hostnameCliente = fgets($fileHostname);
$aula = substr($hostnameCliente, 0, 3);
$hostnameCliente = substr($hostnameCliente, 0, 9); // con esto le quitamos el espacio que se le añade al final.

/* Incluimos el autoload del aula y el del equipo (si existe) */
include "/opt/opengnsys/www/menus/autoloads/$aula/autoload_$aula.php"; 
include "/opt/opengnsys/www/menus/autoloads/$aula/autoload_$hostnameCliente.php";

// Con el include cargamos, entre otras cosas, el número de botones
if ($numeroBotonesPublico==2) {

	// Introducimos la función (script) a ejecutar por cada botón
	$funcionBotonIzquierda = "SincronizarGitUGR 1 2 $imgGeneral $tiempoSincroGeneral";
	$funcionBotonDerecha = "SincronizarGitUGR 1 1 $imgLinux $tiempoSincroLinux";

	// asignamos el tooltip a cada botón
	$tooltipBotonIzquierda = $tooltipWindowsGeneral;
	$tooltipBotonDerecha = $tooltipLinux;

	// Asignamos la imagen a cada botón.
	$iconoBotonIzquierda = $iconoWindowsGeneral;
	$iconoBotonDerecha = $iconoLinux;


} elseif ($numeroBotonesPublico==3) {

	// Introducimos la función (script) a ejecutar por cada botón
	$funcionBotonSuperior = "SincronizarGitUGR 1 2 $imgGeneral $tiempoSincroGeneral";
	$funcionBotonIzquierda = "SincronizarGitUGR 1 3 $imgCentro $tiempoSincroCentro";
	$funcionBotonDerecha = "SincronizarGitUGR 1 1 $imgLinux $tiempoSincroLinux";

	// asignamos el tooltip a cada botón
	$tooltipBotonSuperior = $tooltipWindowsGeneral;
	$tooltipBotonIzquierda = $tooltipWindowsGeneralLocal;
	$tooltipBotonDerecha = $tooltipLinux;

	// Asignamos la imagen a cada botón.
	$iconoBotonSuperior = $iconoWindowsGeneral;
	$iconoBotonIzquierda = $iconoWindowsCentro;
	$iconoBotonDerecha = $iconoLinux;

}


// Metemos el nombre del menú privado que irá asociado a este menú
$menuPrivado = "MenuPrivadoUGR.php";

// El nombre del menú público se consulta así:
$menuPublico = basename($_SERVER['PHP_SELF']);


?>

<!--Doctype HTML5-->
<!DOCTYPE>
<html>


<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>Aulas de informática - Universidad de Granada - CSIRC</title>

	<link rel="stylesheet" type="text/css" href="css/estilos_publico.css"/>

	<?PHP if ($numeroBotonesPublico==2){?> 
		<link rel="stylesheet" type="text/css" href="css/estilos_publico_2botones.css"/>
	<?PHP } elseif ($numeroBotonesPublico==3){?>
		<link rel="stylesheet" type="text/css" href="css/estilos_publico_3botones.css"/>
	<?PHP }?>


</head>

   <body>

   	<h1> </h1>	

   	<?PHP if ($numeroBotonesPublico==3){?> 
   	<dl class="botonSuperior">
		<dt><?PHP echo "<a href='command:$funcionBotonSuperior' title='$tooltipBotonSuperior'><img src='images/$iconoBotonSuperior'></a>";?></dt>
	</dl>
	<?PHP } ?>

	<?PHP if (($numeroBotonesPublico==2)||($numeroBotonesPublico==3)){?> 
	<dl class="botonIzquierda">
		<dt><?PHP echo "<a href='command:$funcionBotonIzquierda' title='$tooltipBotonIzquierda'><img src='images/$iconoBotonIzquierda'></a>";?></dt>

	<dl class="botonDerecha">
		<dt><?PHP echo "<a href='command:$funcionBotonDerecha' title='$tooltipBotonDerecha'><img src='images/$iconoBotonDerecha'></a>";?></dt>				
	</dl>

	</dl>
	<?PHP } ?>

	<dl class="administrador">
		<dt><form id="administrador" action="AutenticacionAdminUGR.php" method="POST">
			<a href="javascript:;" onclick="javascript:document.getElementById('administrador').submit();" title="Ir a Administración"><img src="images/mantenimiento.png"></a>
		</form></dt>

		<dl class="apagar">
			<dt><a href="command:poweroff" title="Apagar el equipo"><img src="images/apagar.png"></a></dt>
		
		<dl class="reiniciar">
			<dt><a href="command:reboot" title="Reiniciar el equipo"><img src="images/reiniciar.png"></a></dt>				
		</dl>

		</dl>
	</dl>

   </body>
</html>
