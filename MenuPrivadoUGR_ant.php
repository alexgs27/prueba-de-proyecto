<?PHP

/* Antes de nada, inicializamos las variables PHP para la correcta configuración 
del menú. Es IMPORTANTE poner el punto y coma después de cada sentencia */

/* Primero incluimos el autoload global */
include '/opt/opengnsys/www/menus/autoloads/autoload.php';

/* A continuación, tenemos que incluir el autoload del aula y el autoload del equipo (en caso de que exista).
Para ello necesitamos saber el nombre del host, que estará almacenado en un fichero con su IP de nombre.
Este fichero se crea en el autoexec arranque general, por lo que es importante que el equipo en cuestión 
tenga configurado el autoexec */
$ipCliente = $_SERVER['REMOTE_ADDR'];
$fileHostname = fopen("/opt/opengnsys/log/clients/hostnames/$ipCliente.hostname","r");
$hostnameCliente = fgets($fileHostname);
$aula = substr($hostnameCliente, 0, 3);
$hostnameCliente = substr($hostnameCliente, 0, 9); // con esto le quitamos el espacio que se le añade al final.

/* Incluimos el autoload del aula y el del equipo (si existe) */
include "/opt/opengnsys/www/menus/autoloads/$aula/autoload_$aula.php"; 
include "/opt/opengnsys/www/menus/autoloads/$aula/autoload_$hostnameCliente.php";

// Vemos si tenemos imagen de centro o no:
if ($hayImagenCentro==TRUE) {
	// En este caso tenemos 3 sistemas operativos
	$numeroSOs = "3";
} else {
	// En este caso tenemos 2 sistemas operativos
	$numeroSOs = "2";
	// Además a $imgCentro le tenemos que poner algo, para que sea reconocido como parámetro por la función descargar.
	$imgCentro = "-";
}

/* Introducimos la función (script) a ejecutar por cada botón (independientemente de que dicho botón se utilize 
en el menú en cuestión o no, ya que los botones se cargarán más adelante en la parte html en función del número de botones) */
//$funcionBotonDescargar = "DescargarGitUGR $numeroSOs $imgLinux $imgGeneral $imgCentro";
$funcionBotonDescargar = "DescargarGitUGR $numeroSOs $imgLinux $imgGeneral $imgCentro EXT3:$tamPartLinux NTFS:$tamPartGeneral NTFS:$tamPartCentro $tamCache";
$funcionBotonRepararLinux = "ReparaImageGitUGR 1 1 $imgLinux";
$funcionBotonRepararGeneral = "ReparaImageGitUGR 1 2 $imgGeneral";
$funcionBotonRepararCentro = "ReparaImageGitUGR 1 3 $imgCentro";
$funcionBotonSubirRegGeneral = "SubirRegistroUGR 2";
$funcionBotonSubirRegCentro = "SubirRegistroUGR 3";
// De momento el arranque desatendido ha sido diseñado para la partición 3:
//$funcionBotonArranqueDesatendido = "PrepararArranqueDesatendidoUGR 3 $imgCentro.dd $tiempoSincroCentro $modoSincro";


// El nombre del menú público (para volver a él):
$menuPublico = "MenuPublicoUGR.php";

?>

<!--Doctype HTML5-->
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>Aulas de informática - Universidad de Granada - CSIRC</title>

	<link rel="stylesheet" type="text/css" href="css/estilos_privado.css"/>

	<?PHP if ($numeroBotonesPrivado==5){?> 
		<link rel="stylesheet" type="text/css" href="css/estilos_privado_5botones.css"/>
	<?PHP } elseif ($numeroBotonesPrivado==6){?>
		<link rel="stylesheet" type="text/css" href="css/estilos_privado_6botones.css"/>
	<?PHP }?>

</head>

   <body>

	<h1> </h1>

	<?PHP //El botón de descargar va a estar siempre, pero dependerá del número de botones para estar en una fila o en otra?>
	<dl class="boton1">
        	<dt><?PHP echo "<a href='commandwithconfirmation:$funcionBotonDescargar' title='$tooltipDescargar'><img src='images/$iconoDescargar'></a>";?></dt>
	</dl>	

	<?PHP //Esto es para el caso de 5 botones
	if ($numeroBotonesPrivado==5){?>

	<dl class="boton2">
		<dt><?PHP echo "<a href='commandwithconfirmation:$funcionBotonCopiarDD' title='$tooltipCopiarDD'><img src='images/$iconoCopiarDD'></a>";?></dt>
	<dl class="boton3">
        	<dt><?PHP echo "<a href='command:$funcionBotonSubirRegGeneral' title='$tooltipSubirRegistroGeneral'><img src='images/$iconoSubirRegGeneral'></a>";?></dt>
    	</dl>		
	</dl>

	<dl class="boton4">
		<dt><?PHP echo "<a href='commandwithconfirmation:$funcionBotonRepararLinux' title='$tooltipRepararLinux'><img src='images/$iconoRepararLinux'></a>";?></dt>
    	<dl class="boton5">
		<dt><?PHP echo "<a href='commandwithconfirmation:$funcionBotonRepararGeneral' title='$tooltipRepararGeneral'><img src='images/$iconoRepararGeneral'></a>";?></dt>
	</dl>		
	</dl>
	<?PHP }?>
	

	<dl class="volver">
		<dt><form id="volver" action=<?PHP echo "$menuPublico"?> method="POST">
			<a href="javascript:;" onclick="javascript:document.getElementById('volver').submit();" title="Volver al menú de usuario"><img src="images/volver.png"></a>
		</form></dt>

		<dl class="apagar">
			<dt><a href="command:poweroff" title="Apagar el equipo"><img src="images/apagar.png"></a></dt>
		<dl class="reiniciar">
				<dt><a href="command:reboot" title="Reiniciar el equipo"><img src="images/reiniciar.png"></a></dt>				
		</dl>
		</dl>
	</dl>

   </body>
</html>
