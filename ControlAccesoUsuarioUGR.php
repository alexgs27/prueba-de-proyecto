<?php

// lineas para mostrar los errores del script
//error_reporting(E_ALL);
//ini_set('display_errors', '1');



/* Metemos en variables el usuario (completo, con la cuenta de correo) 
y la contraseña que hemos introducido en el formulario en AutenticacionUsuarioUGR.php */
$usuario = $_POST["usuario"];
$contrasena = $_POST["contrasena"];
//$usuario = "usuario" ;
//$contrasena = "tatata" ;


/* Definimos una funcion para guardar el usuario que ha accedido de forma correcta, este usuario
quedara registrado en los logs de arranques. Solo se guarda el usuario, nunca la contrasena */

function saveUser($usuario)
{
	$ipCliente = $_SERVER['REMOTE_ADDR']; // IP del PC cliente
	$fileUser = fopen ("/opt/opengnsys/log/clients/users/$ipCliente.user","w"); // archivo en el que se guardara el usuario
	fwrite($fileUser,$usuario . PHP_EOL);
	fclose($fileUser);
}

/* Definimos una funcion para modificar el contenido del fichero arranquefile, sustituyendo el usuario generico "aulas" por el
usuario que se haya autenticado correctamente  y ademas se inserta la hora de autenticacion del usuario, sustituyendo a la del 
arranque del equipo*/

function ajusteLog($usuario)
{
        //Definicion de variables
        $ipCliente = $_SERVER['REMOTE_ADDR']; // IP del PC cliente
        $info = "<\INFO>" ; //metemos la barra para mantener los caracteres mayor y menor , por bash se quita antes de llegar al log de arranques
        $fileHostname = fopen("/opt/opengnsys/log/clients/hostnames/$ipCliente.hostname","r"); //este fichero lo crea autoexec
        $hostnameCliente = fgets($fileHostname);
        $hostCliente = trim($hostnameCliente); //quitamos el retorno de carro
        $nombreImagen = "IMAGEN" ; //valor generico
        $menuHTML = "PUBLICO.HTML" ; //valor generico
        $funcion = "NINGUNO" ; // valor generico
        $ipServer = $_SERVER['SERVER_ADDR']; // IP del servidor 
        $tipoArranque = "NINGUNO" ; // valor por defecto
        $horaActual = date("Y/m/d H:i:s");
        
	//Se construye la nueva linea de log
//        $lineaNueva ="[".$horaActual."]"." ".$ipCliente." ".$info." ".$horaActual.",".$horaActual.",".$horaActual.",".$hostCliente.",".$nombreImagen.",".$menuHTML.",".$usuario.",".$funcion.",".$ipServer.",".$tipoArranque."\r";
// se corrige el formato para que no meta fin de linea de windows

	$lineaNueva ="[".$horaActual."]"." ".$ipCliente." ".$info." ".$horaActual.",".$horaActual.",".$horaActual.",".$hostCliente.",".$nombreImagen.",".$menuHTML.",".$usuario.",".$funcion.",".$ipServer.",".$tipoArranque;
       // echo $lineaNueva ;
        
	//abro un fichero para actualizar el contenido del arranquefile
         $fileArranque = fopen("/opt/opengnsys/log/clients/arranquefiles/$ipCliente.arranquefile.user","w");
        //guardo la linea nueva
        fwrite($fileArranque, $lineaNueva . PHP_EOL);
         //cierro fichero
        fclose($fileArranque);  
}




/* Comprobamos de que tipo de cuenta se trata, viendo si lleva al final el ".docencia" */
$isDocencia = substr_count($usuario, ".docencia");

if ($isDocencia == 1) {
	$posicion = stripos($usuario, ".docencia");
	$usuario = substr_replace($usuario,"",$posicion);
}


/* Cuando la cuenta de correo o la contraseña son erróneos, por defecto tardaba unos 15 segundos 
en darse cuenta de que algo va mal. En principio, pensabamos que esto era debido al timeout, 
pero resulta que no tiene nada que ver con esto. El problema es que por defecto intenta 
la apertura 3 veces, y en cada una tarda unos 5 segundos. Hay que decirle que lo intente una sola vez, 
y en este caso tardará sólamente 5 segundos en total (un tiempo razonable) 
https://bugs.php.net/bug.php?id=39362 */


//vemos que tipo de cuenta de correo utilizamos y luego vemos si el usuario y contraseña es válildo a través de imap
if ($isDocencia==1) {
	// En este caso tenemos una cuenta de Alumno
	if ($mbox = imap_open('{correo.ugr.es:993/imap/ssl/novalidate-cert}', $usuario, $contrasena,null,0)) {
   		header ("Location: MenuPublicoUGR.php");

	//en caso de autenticacion correcta, se guarda el usuario que ha accedido, añadiendo .docencia para q se refleje en log
	$usuarioDocencia = $usuario.".".docencia ;
	
	saveUser($usuarioDocencia);
	//y ademas se refleja en arranquefile
	ajusteLog($usuarioDocencia);
	}
	else {
	    header ("Location: AutenticacionUsuarioUGR.php?errorauth=error");
	}
}
else if ($isDocencia==0) {
	// Usuario administrador para poder entrar con una cuenta que no sea de la UGR, QUITAR en un futuro. 
	if ($usuario=="administrador" && $contrasena=="patata") {
		// En este caso hemos añadido una exepción, para que podamos entrar con una cuenta que no sea de la UGR
		header ("Location: MenuPublicoUGR.php");
	
		//en caso de autenticacion correcta, se guarda el usuario que ha accedido
        	saveUser($usuario);
		//y ademas se refleja en arranquefile
		ajusteLog($usuario);
	}
	// En este caso tenemos una cuenta de PAS o de Profesorado
	else if ($mbox = imap_open('{imap.ugr.es:993/imap/ssl/novalidate-cert}', $usuario, $contrasena,null,0)) {
   		header ("Location: MenuPublicoUGR.php");
		//en caso de autenticacion correcta, se guarda el usuario que ha accedido
        	saveUser($usuario);
		//y ademas se refleja en arranquefile
		ajusteLog($usuario);
	}
	else {
	   header ("Location: AutenticacionUsuarioUGR.php?errorauth=error");
	}
}
else {
	header ("Location: AutenticacionUsuarioUGR.php?errorauth=error");
}


// Se cierra la conexión imap, una vez que se ha validado autenticación
imap_close($mbox);
?>
