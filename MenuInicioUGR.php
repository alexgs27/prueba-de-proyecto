<?PHP

/* Se cargan las variables PHP para determinar si el aula o el PC requeiren autenticacion de usuario o se accede directamente al menu publico.
   Se cargan los ficheros autoloads, aunque la mayoria de las variables no se necesitan en este script */

/* Primero incluimos el autoload global */
include '/opt/opengnsys/www/menus/autoloads/autoload.php';

/* A continuación, tenemos que incluir el autoload del aula y el autoload del equipo (en caso de que exista).
Para ello necesitamos saber el nombre del host, que estará almacenado en un fichero con su IP de nombre.
Este fichero se crea en el autoexec arranque general, por lo que es importante que el equipo en cuestión 
tenga configurado el autoexec */
$ipCliente = $_SERVER['REMOTE_ADDR'];
$fileHostname = fopen("/opt/opengnsys/log/clients/hostnames/$ipCliente.hostname","r");
$hostnameCliente = fgets($fileHostname);
$aula = substr($hostnameCliente, 0, 3);
$hostnameCliente = substr($hostnameCliente, 0, 9); // con esto le quitamos el espacio que se le añade al final.

/* Incluimos el autoload del aula y el del equipo (si existe) */
include "/opt/opengnsys/www/menus/autoloads/$aula/autoload_$aula.php";
include "/opt/opengnsys/www/menus/autoloads/$aula/autoload_$hostnameCliente.php";

/*funcion para retocar arranquefile en el caso de PCs sin acceso identificado  */

function ajusteLog ()
{
$ipCliente = $_SERVER['REMOTE_ADDR'];
$fileArranque = fopen("/opt/opengnsys/log/clients/arranquefiles/$ipCliente.arranquefile","r");
$linea = fgets($fileArranque);
$linea = str_replace("DEFECTO.HTML","PUBLICO.HTML",$linea);
#fwrite($fileArranque,$linea . PHP_EOL);
fclose($fileArranque);

$fileArranque2 = fopen("/opt/opengnsys/log/clients/arranquefiles/$ipCliente.arranquefile2","w");
fwrite($fileArranque2,$linea);
fclose($fileArranque2);

rename("/opt/opengnsys/log/clients/arranquefiles/$ipCliente.arranquefile2","/opt/opengnsys/log/clients/arranquefiles/$ipCliente.arranquefile");

}




/* Evaluo la variable para determinar cual sera la primera pagina que se le mande al PC */

if ($accesoIdentificado)
{
 // echo 'redirecciona a la ventana de autentificacion';
  echo '<html><head><META HTTP-EQUIV="REFRESH" CONTENT="1;URL=https://172.18.133.195/opengnsys/menus/AutenticacionUsuarioUGR.php"></head></html>';

}else {
  //corrige la linea de log
	ajusteLog(); 

  // echo 'redirecciona al menu publico';
  echo '<html><head><META HTTP-EQUIV="REFRESH" CONTENT="1;URL=https://172.18.133.195/opengnsys/menus/MenuPublicoUGR.php"></head></html>';
}

?>

