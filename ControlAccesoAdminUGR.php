<?php
ini_set("display_errors",1);
error_reporting(E_ALL);

//Definimos una funcion para almacenar el usuario en el caso de autenticacion correcta, este usuario quedara reflejado en el log de arranques.

function saveUser($usuario)
{
        $ipCliente = $_SERVER['REMOTE_ADDR']; // IP del PC cliente
        $fileUser = fopen ("/opt/opengnsys/log/clients/users/$ipCliente.user.admin","w"); // archivo en el que se guardara el usuario
        fwrite($fileUser,$usuario . PHP_EOL);
        fclose($fileUser);
}

function ajusteLog($usuario)
{
        //Definicion de variables
        $ipCliente = $_SERVER['REMOTE_ADDR']; // IP del PC cliente
        $info = "<\INFO>" ; //metemos la barra para mantener los caracteres mayor y menor , por bash se quita antes de llegar al log de arranques
        $fileHostname = fopen("/opt/opengnsys/log/clients/hostnames/$ipCliente.hostname","r"); //este fichero lo crea autoexec
        $hostnameCliente = fgets($fileHostname);
        $hostCliente = trim($hostnameCliente); //quitamos el retorno de carro
        $nombreImagen = "IMAGEN" ; //valor generico
        $menuHTML = "PRIVADO.HTML" ; //valor generico
        $funcion = "NINGUNO" ; // valor generico
        $ipServer = $_SERVER['SERVER_ADDR']; // IP del servidor 
        $tipoArranque = "NINGUNO" ; // valor por defecto
        $horaActual = date("Y/m/d H:i:s");

        //Se construye la nueva linea de log
	//        $lineaNueva ="[".$horaActual."]"." ".$ipCliente." ".$info." ".$horaActual.",".$horaActual.",".$horaActual.",".$hostCliente.",".$nombreImagen.",".$menuHTML.",".$usuario.",".$funcion.",".$ipServer.",".$tipoArranque."\r";
	// se corrige el formato para que no inserte retorno de linea de windows
	$lineaNueva ="[".$horaActual."]"." ".$ipCliente." ".$info." ".$horaActual.",".$horaActual.",".$horaActual.",".$hostCliente.",".$nombreImagen.",".$menuHTML.",".$usuario.",".$funcion.",".$ipServer.",".$tipoArranque;
       // echo $lineaNueva ;

        //abro un fichero para actualizar el contenido del arranquefile
         $fileArranque = fopen("/opt/opengnsys/log/clients/arranquefiles/$ipCliente.arranquefile.admin","w");
        //guardo la linea nueva
        fwrite($fileArranque, $lineaNueva . PHP_EOL);
         //cierro fichero
        fclose($fileArranque);
}


// Aquí creamos la lista de usuarios autorizados. En caso de ser cuenta de alumno, se añade SIN el ".docencia"
$usuariosAutorizados = array("promera","mrpareja","isvida","jroman","acanoru","felisa","cabreriz","leire","isabelmonte");

//Se inicializan variables
$usuarioValido = false;

/* Metemos en variables el usuario y la contraseña que hemos 
introducido en el formulario en AutenticacionAdminUGR.php */
$usuario = $_POST["usuario"];
$contrasena = $_POST["contrasena"];


/* Comprobamos de que tipo de cuenta se trata, viendo si lleva al final el ".docencia" */
$isDocencia = substr_count($usuario, ".docencia");

if ($isDocencia == 1) {
	$posicion = stripos($usuario, ".docencia");
	$usuario = substr_replace($usuario,"",$posicion);
}

/* Se recorre el array de usuarios autorizados y se comprueba si el usuario que hemos introducido lo está. */
foreach ($usuariosAutorizados as $valor) {
	if ($usuario==$valor) {
		$usuarioValido = true;
		break;
	}
}

/* Si el usuario está autorizado, pasaríamos al intento de autenticación contra IMAP. 
 En caso contrario se indica que el usuario no está autorizado. */
if ($usuarioValido==true) {

	//vemos que tipo de cuenta de correo utilizamos y luego vemos si el usuario y contraseña es válildo a través de imap
	if ($isDocencia==1) {

		// En este caso tenemos una cuenta de Alumno
		if ($mbox = imap_open('{correo.ugr.es:993/imap/ssl}', $usuario, $contrasena,null,0)) {
	   		header ("Location: MenuPrivadoUGR.php");
			//si el usuario accede al menu privado, lo guardamos para el log
			saveUser($usuario);
			 //y ademas se refleja en arranquefile
                	ajusteLog($usuario);

		}
		else {
		    header ("Location: AutenticacionAdminUGR.php?errorauth=SiValido");
		}
	}
	else if ($isDocencia==0) 
	{
		// En este caso tenemos una cuenta de PAS o de Profesorado
		if ($mbox = imap_open('{imap.ugr.es:993/imap/ssl}', $usuario, $contrasena,null,0)) {

	   		header ("Location: MenuPrivadoUGR.php");
			//si el usuario accede al menu privado, lo guardamos para el log
                        saveUser($usuario);
			  //y ademas se refleja en arranquefile
                        ajusteLog($usuario);

		}
		else {
		   header ("Location: AutenticacionAdminUGR.php?errorauth=SiValido");
		}
	}

	// Se cierra la conexión imap, una vez que se ha validado autenticación
	imap_close($mbox);
}
else if ($usuario=="administrador" && $contrasena=="patata") {
	// En este caso hemos añadido una exepción, para que podamos entrar con una cuenta que no sea de la UGR
	header ("Location: MenuPrivadoUGR.php");
	//si el usuario accede al menu privado, lo guardamos para el log
         saveUser($usuario);
	  //y ademas se refleja en arranquefile
         ajusteLog($usuario);

}
else {
	// En este caso no se trata de un usuario válido
	header ("Location: AutenticacionAdminUGR.php?errorauth=NoValido");
}


/* Cuando la cuenta de correo o la contraseña son erróneos, por defecto tardaba unos 15 segundos 
en darse cuenta de que algo va mal. En principio, pensabamos que esto era debido al timeout, 
pero resulta que no tiene nada que ver con esto. El problema es que por defecto intenta 
la apertura 3 veces, y en cada una tarda unos 5 segundos. Hay que decirle que lo intente una sola vez, 
y en este caso tardará sólamente 5 segundos en total (un tiempo razonable) 
https://bugs.php.net/bug.php?id=39362 */


?>
