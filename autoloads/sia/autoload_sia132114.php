<?php

$numeroBotonesPublico = 3;
$numeroBotonesPrivado = 6;

// Indicamos el nombre la las imagenes, sin .img ni .dd
$imgLinux = "Glx16_647";
$imgGeneral = "Gw747";
$imgCentro = "win764_valen";

$hayImagenCentro = TRUE; 

// Tamaño de las particiones y la cache en GB:
$tamPartLinux = "15";
$tamPartGeneral = "35";
$tamPartCentro = "35";
$tamCache = "70";

$modoDescarga = "UNICAST";

$tiempoSincroLinux = "180";
$tiempoSincroGeneral = "230";
$tiempoSincroCentro = "185";

// Modo de sincronización:
$modoSincro = "REPO";

$hayArranqueDesatendido = FALSE;

$accesoIdentificado = FALSE;

?>

