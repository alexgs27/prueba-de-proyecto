<?php

/* NOTA: Es muy IMPORTANTE que todas las declaraciones acaben en ";", si no, no funcionará 
correctamente. También es imprescindible que las variables empiezen por "$".
Todas las variables (excepto el número de botones y los booleanos) se ponen como string para ser pasadas 
correctamente como parámetros a los script. OJO con las mayúsculas y minúsculas. */

/* Se indica que numero de botones tienen los menús publico y privado. 
Los botones de apagar, reiniciar y mantenimiento no se cuentan, porque son fijos.
Podemos tener de 2 a 5 botones en el público y 4 o 6 botones en el privado. */
$numeroBotonesPublico = 2;
$numeroBotonesPrivado = 4;

// Indicamos el nombre la las imagenes, sin .img ni .dd
$imgLinux = "imagenLinux";
$imgGeneral = "Gw711";
$imgCentro = "";

// También tenemos un booleano para saber si tenemos imagen de centro o no,
// ya que hay aulas que no tienen imagen de centro.
// Esta variable es muy IMPORTANTE ponerla a TRUE si tenemos imagen de Centro, en caso contrario será ignorada.
$hayImagenCentro = FALSE;

// Tamaño de las particiones y la cache en GB:
$tamPartLinux = "30";
$tamPartGeneral = "30";
$tamPartCentro = "30";
$tamCache = "100";

// Modo de descarga (UNICAST, MULTICAST, TORRENT, UNICAST-DIRECT, MULTICAST-DIRECT)
// El TORRENT aún está pendiente de ser estudiado.
$modoDescarga = "UNICAST";

/* En caso del Multicast, tendremos que poner sus opciones, que serían la IP Multicast, 
el número de clientes y el tiempo de espera para empezar una sesión,
con este formato: <IpMulticas>:<NumeroClientes>:<TiempoEspera>.
En el caso del Torrent se utilizará esta misma variable para sus opciones. */
$opcionesMulticast = "";
// Este sería un ejemplo de opciones para Multicast en la subred 172.18.132.0, 
// para 20 equipos y con un tiempo de espera de 60 segundos:
//$opcionesMulticast = "239.194.18.132:20:60";

// Se indica el "tooltip" o mensaje emergente descriptivo de cada botón
$tooltipWindowsCentro = "Iniciar sesión de  Windows Centro";
$tooltipWindowsCentroLocal = "Iniciar sesión Local de  Windows Centro";
$tooltipWindowsGeneral = "Iniciar sesión de  Windows General";
$tooltipWindowsGeneralLocal = "Iniciar sesión Local de  Windows General";
$tooltipLinux = "Iniciar sesión de Ubuntu ";
$tooltipDescargar = "Particionar, formatear el equipo e instalar los sistemas operativos en las particiones";  
$tooltipParticionar = "Particionar y formatear el equipo";                      // QUITAR si no se utiliza.
$tooltipRepararLinux = "Instalar Ubuntu en la partición 1";
$tooltipRepararGeneral = "Instalar Windows General en la partición 2"; 
$tooltipRepararCentro = "Instalar Windows Centro en la partición 3"; 
$tooltipSubirRegistroGeneral = "Subir el registro de Windows General de la partición 2 al repositorio"; 
$tooltipSubirRegistroCentro = "Subir el registro de Windows Centro de la partición 3 al repositorio";
$tooltipArranqueDesatendido = "Preparar equipo para el arranque desatendido";

// Nombre de la imagen (archivo .PNG) de cada botón. Se les llaman iconos 
// para no confundirlos con imágenes de sistemas operativos.
$iconoWindowsGeneral = "winGeneral.png";
$iconoWindowsLocal = "winLocal.png";
$iconoWindowsCentro = "winCentro.png";
$iconoLinux = "linux.png";
$iconoDescargar = "descargar.png";
$iconoParticionar = "particionar.png";   // QUITAR si no se utiliza.
$iconoRepararLinux = "linuxR.png";
$iconoRepararGeneral = "repararWin.png";
$iconoRepararCentro = "repararWin.png";
$iconoSubirRegGeneral = "subirRegistroWin.png";
$iconoSubirRegCentro = "subirRegistroWin.png";
$iconoArranqueDesatendido = "arranqueDesatendido.png";

// Tiempo de sincronizacion estimado, que nos sirve 
// para la barra de progreso.
$tiempoSincroLinux = "120";
$tiempoSincroGeneral = "150";
$tiempoSincroCentro = "180";

// Modo de sincronización: 
$modoSincro = "REPO";

// Para saber si tenemos arranque desatendido, habrá una variable que normalmente estará a FALSE.
// En el aula que queramos botón de arranque desatendido, habrá que poner esta variable a TRUE.
$hayArranqueDesatendido = FALSE;

// Para determinar si hay autenticacióe usuario, se usa la sigueinte variable, en este nivel esta a FALSE

$accesoIdentificado = FALSE; 



?>
